import { useState } from "react";
import "./styles/header.css";

export const Header = () => {

	const [isOpen, setIsOpen] = useState<boolean>(false);

	const handleChange = () => {
		setIsOpen(!isOpen)
		if (isOpen) {
			document.body.style.overflowY = "auto"
			document.querySelector("#header__hamburger")?.setAttribute("aria-expanded", "false")
		} else {
			document.body.style.overflowY = "hidden"
			document.querySelector("#header__hamburger")?.setAttribute("aria-expanded", "true")
		}
	}

	return (
		<>
			<header className="header container">
				<nav>
					<ul className="header__menu">
						<li>
							<a className="header__link" href="#about">About</a>
						</li>
						<li>
							<a className="header__link" href="#education">Education</a>
						</li>
						<li>
							<a className="header__link" href="#work-experience">Work Experience</a>
						</li>
						<li>
							<a className="header__link" href="#skills">Skills</a>
						</li>
						<li>
							<a className="header__link" href="#hobbies">Hobbies</a>
						</li>
						<li>
							<a className="header__link" href="#portfolio">Portfolio</a>
						</li>
						<li>
							<a className="header__link" href="#contact">Contact</a>
						</li>
						<li className="header__line"></li>
						<li aria-label="home icon" className="header__home-icon">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
								<path d="M11.47 3.841a.75.75 0 0 1 1.06 0l8.69 8.69a.75.75 0 1 0 1.06-1.061l-8.689-8.69a2.25 2.25 0 0 0-3.182 0l-8.69 8.69a.75.75 0 1 0 1.061 1.06l8.69-8.689Z" />
								<path d="m12 5.432 8.159 8.159c.03.03.06.058.091.086v6.198c0 1.035-.84 1.875-1.875 1.875H15a.75.75 0 0 1-.75-.75v-4.5a.75.75 0 0 0-.75-.75h-3a.75.75 0 0 0-.75.75V21a.75.75 0 0 1-.75.75H5.625a1.875 1.875 0 0 1-1.875-1.875v-6.198a2.29 2.29 0 0 0 .091-.086L12 5.432Z" />
							</svg>
						</li>
					</ul>
					<button onClick={handleChange} className="header__hamburger" id="header__hamburger" aria-expanded="false">
						<svg stroke="currentColor" strokeWidth="8" fill="none" viewBox="0 0 100 115">
							<path className="header__hamburger-line" strokeLinecap="round" strokeLinejoin="round" d="m 20 30 h 60 a 1 1 0 0 1 0 20 h -60 a 1 1 0 0 0 0 20 h 60 a 1 1 0 0 1 0 20 h -30 v -80" />
						</svg>
					</button>
				</nav>
			</header>
			<div className="mobile-nav" style={{ transform: isOpen ? "translateY(0)" : "translateY(-100%)" }}>
				<nav>
					<ul className="mobile-nav__menu">
						<li onClick={handleChange} aria-label="home icon" className="mobile-nav__home-icon">
							<a className="mobile-nav__link" href="#">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
									<path d="M11.47 3.841a.75.75 0 0 1 1.06 0l8.69 8.69a.75.75 0 1 0 1.06-1.061l-8.689-8.69a2.25 2.25 0 0 0-3.182 0l-8.69 8.69a.75.75 0 1 0 1.061 1.06l8.69-8.689Z" />
									<path d="m12 5.432 8.159 8.159c.03.03.06.058.091.086v6.198c0 1.035-.84 1.875-1.875 1.875H15a.75.75 0 0 1-.75-.75v-4.5a.75.75 0 0 0-.75-.75h-3a.75.75 0 0 0-.75.75V21a.75.75 0 0 1-.75.75H5.625a1.875 1.875 0 0 1-1.875-1.875v-6.198a2.29 2.29 0 0 0 .091-.086L12 5.432Z" />
								</svg>
							</a>
						</li>
						<li className="mobile-nav__line"></li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#about">About</a>
						</li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#education">Education</a>
						</li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#work-experience">Work Experience</a>
						</li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#skills">Skills</a>
						</li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#hobbies">Hobbies</a>
						</li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#personal-projects">Personal Projects</a>
						</li>
						<li onClick={handleChange}>
							<a className="mobile-nav__link" href="#contact">Contact</a>
						</li>
					</ul>
				</nav>
			</div>
		</>
	);
};
