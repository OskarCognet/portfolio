import { CSSProperties, MouseEventHandler, ReactNode } from "react"
import "./styles/button.css"

export const Button = ({ className, onClick, style, children }: { className?: string, onClick?: MouseEventHandler<HTMLButtonElement>, style?: CSSProperties, children: ReactNode }) => {

    return (
        <button className={className + " component__Button"} onClick={onClick} style={style}>
            {children}
        </button>
    )
}