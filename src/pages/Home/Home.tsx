import { useState } from "react"
import { Button } from "../../components/Button"
import { Header } from "../../components/Header"
import { About } from "./components/About"
import { Contact } from "./components/Contact"
import { Hero } from "./components/Hero"
import { Hobbies } from "./components/Hobbies/Hobbies"
import "./home.css"

export const Home = () => {

    // Update width of bugged element to fix it.
    setTimeout(() => {
        const bugged_dude = document.getElementById("panel1-heading");
        console.log("inside setTimeout")
        if (bugged_dude) {
            bugged_dude.style.width = "auto";
            console.log("changed width of bugged dude")
        }
    }, 1);

    const [visible, setVisible] = useState(false)

    const toggleVisible = () => {
        const scrolled = document.documentElement.scrollTop;
        if (scrolled > 600) {
            setVisible(true)
        }
        else {
            setVisible(false)
        }
    };
    window.addEventListener('scroll', toggleVisible);

    const scrollToTop = () => {
        window.scrollTo({
            top: 0,
        });
    };

    return (
        <>
            <Header />
            <main>
                <Hero />
                <div className="spacer layer_purple-to-orange"></div>
                <About />
                <div className="spacer layer_orange-to-green"></div>
                <Hobbies />
                <div className="spacer layer_green-to-purple"></div>
                <Contact />
                <Button className="home__return-top" onClick={scrollToTop} style={{ transform: visible ? "scale(1)" : "scale(0)" }}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24" className="home__return-top-svg" stroke="#17001a" fill="#17001a">
                        <path d="M440-608 324-492q-11 11-28 11t-28-11q-11-11-11-28t11-28l184-184q12-12 28-12t28 12l184 184q11 11 11 28t-11 28q-11 11-28 11t-28-11L520-608v328q0 17-11.5 28.5T480-240q-17 0-28.5-11.5T440-280v-328Z" />
                    </svg>
                </Button>
            </main>
            <footer></footer>
        </>
    )
}