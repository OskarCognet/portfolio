import { useState } from "react";
import "./hobbies.css"

export const Hobbies = () => {

    const [, setExpanded] = useState<string>("gaming")

    const panels = document.querySelectorAll(".hobbies__accordion-panel")

    const handleClick = (name: string) => {
        setExpanded(name)
        panels.forEach(panel => {
            if (panel.className.includes(name)) {
                panel.firstElementChild?.firstElementChild?.setAttribute("aria-expanded", "true")
            } else {
                panel.firstElementChild?.firstElementChild?.setAttribute("aria-expanded", "false")
            }
        });
    }

    return (
        <div className="bg-green">
            <section id="hobbies" className="hobbies container">
                <h2 className="hobbies__title">Hobbies</h2>
                <div className="hobbies__accordion">

                    <div className="hobbies__accordion-panel cooking" onClick={() => handleClick("cooking")}>
                        <h3 id="panel1-heading" className="hobbies__panel-heading">
                            <button
                                className="hobbies__accordion-trigger"
                                aria-controls="panel1-content"
                                aria-expanded="true"
                            >
                                <div className="hobbies__accordion-icon-container">
                                    <svg stroke="currentColor" viewBox="0 0 100 100" className="hobbies__accordion-icon">
                                        <g className="hobbies__accordion-icon__cooking-whisk" strokeLinecap="round" strokeLinejoin="round" strokeWidth="6" fill="none">
                                            <path d="m52.5 68.9c1.92-7.08e-4 3.47 1.72 3.47 3.85l-0.0071 19.3c-7.86e-4 2.14-1.55 3.86-3.47 3.86s-3.47-1.72-3.47-3.85l0.0071-19.3c7.86e-4 -2.14 1.55-3.86 3.47-3.86z" />
                                            <path strokeWidth="4" d="m52.5 71.1s42.5-65-0.0165-65" />
                                            <path strokeWidth="4" d="m52.4 71.1s-42.5-65 0.0644-65" />
                                            <path strokeWidth="4" d="m52.5 68.9s15.9-62.8-1e-6 -62.8" />
                                            <path strokeWidth="4" d="m52.5 68.9s-15.9-62.8 1e-6 -62.8" />
                                        </g>
                                        <g className="hobbies__accordion-icon__cooking-chefhat" fill="none" strokeWidth="6" strokeLinecap="round" strokeLinejoin="round">
                                            <rect x="23.3" y="70" width="54.2" height="15.3" ry="6.34" />
                                            <path d="m73.4 31c-7.06-26.8-39.5-26.8-46.6 0" />
                                            <path d="m28.2 70c-20.3-5.77-21.3-49.5 3.7-49.5" />
                                            <path d="m72.4 70c20.3-5.77 21.3-49.5-3.7-49.5" />
                                            <g transform="matrix(1.02 0 0 1.06 -122 -32.4)">
                                                <path d="m169 96v-12.1" />
                                                <path d="m160 96c-4.96-2-9.67-7.18-11.6-12.6" />
                                                <path d="m178 96c4.96-2 9.67-7.18 11.6-12.6" />
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <span id="panel1-title" className="hobbies__panel-title">Cooking</span>
                            </button>
                        </h3>
                        <div
                            className="hobbies__accordion-content"
                            id="panel1-content"
                            aria-labelledby="panel1-heading"
                            aria-hidden="true"
                            role="region"
                        >
                            <div className="hobbies__inside-content">
                                <p>
                                    I love cooking!  Mostly because I love eating, who doesn't? But for me, cooking isn't only about sustenance, it's about <strong className="hobbies__strong">sharing and bonding</strong> with people.
                                </p>
                                <p>
                                    I get fulfillment out of designing dishes exactly how I enjoy them and sharing <strong className="hobbies__strong">my vision of cuisine</strong> with people I care about. I grew up where dinner and lunch were places of gathering and bonding. Making those daily but important events special with hand-crafted meals is in my opinion a <strong className="hobbies__strong">game changer</strong> in people's everyday life.
                                </p>
                                <p>
                                    For special events, I have my special recipes. Coming from the French Alps, I have a deep love for all things cheese: tartiflette, raclette, fondue, and lasagnas! Those will turn any meal into <strong className="hobbies__strong">a true feast!</strong>
                                </p>
                                <p>
                                    Enough about me though! What are your favorite recipes? Tell me about your <strong className="hobbies__strong">best kitchen memories</strong>, mine are about meals that went horribly wrong 😅!
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="hobbies__accordion-panel skiing" onClick={() => handleClick("skiing")}>
                        <h3 id="panel2-heading" className="hobbies__panel-heading">
                            <button
                                className="hobbies__accordion-trigger"
                                aria-controls="panel2-content"
                                aria-expanded="false"
                            >
                                <div className="hobbies__accordion-icon-container">
                                    <svg viewBox="0 0 100 100" stroke="currentColor" className="hobbies__accordion-icon">
                                        <g strokeWidth="7" strokeLinecap="round" strokeLinejoin="round">
                                            <path className="hobbies__accordion-icon__ski-ski" fill="none" d="M 85 66 l -50 20 a 1 1 0 0 1 -10 -25 l 18 -6 l 2 -35 a 2 4 0 0 0 -15 0 l 2 35 l -2 25 a 5 4 0 0 0 15 0 l -2 -25" />
                                            <path className="hobbies__accordion-icon__ski-body" fill="none" d="M 60 76 l -20 -20 l 20 -10 l -30 -20 l 10 20 l -20 0 l 49 -20 l 1 -6 a 2 4 0 0 0 -15 0 l 2 35 l -2 25 a 5 4 0 0 0 15 0 l -2 -25 l 2 -35" />
                                            <path className="hobbies__accordion-icon__ski-sticktip" fill="none" d="M 62 23 l 4 10" />
                                            <path className="hobbies__accordion-icon__ski-stick1" fill="none" d="M 15 83 l 0 -50 M 10 77 l 10 0" strokeWidth="5" />
                                            <path className="hobbies__accordion-icon__ski-stick1" fill="none" d="M 15 35 l 0 -10" strokeWidth="8" />
                                            <path className="hobbies__accordion-icon__ski-stick1" fill="none" d="M 85 83 l 0 -50 M 80 77 l 10 0" strokeWidth="5" />
                                            <path className="hobbies__accordion-icon__ski-stick1" fill="none" d="M 85 35 l 0 -10" strokeWidth="8" />
                                            <circle className="hobbies__accordion-icon__ski-head" cx="25" cy="20" r="7" />
                                        </g>
                                    </svg>
                                </div>
                                <span id="panel2-title" className="hobbies__panel-title">Skiing</span>
                            </button>
                        </h3>
                        <div
                            className="hobbies__accordion-content"
                            id="panel2-content"
                            aria-labelledby="panel2-heading"
                            aria-hidden="true"
                            role="region"
                        >
                            <div className="hobbies__inside-content">
                                <p>
                                    I've been skiing ever since I can remember it. My father is a big fan of skiing, so he naturally taught my brother and I his passion. He was a tough teacher and we didn't love every step of the process, but here we are today, fantastic skiers <strong className="hobbies__strong">sharing his passion.</strong>
                                </p>
                                <p>
                                    Today, I love skiing for so many reasons, the <strong className="hobbies__strong">beautiful scenery</strong>, the physical and technical challenge, the time spent with loved ones and all the <strong className="hobbies__strong">memories</strong> that comes with it. Today I understand why my father is so passionate about skiing, and hopefully one day I'll be <strong className="hobbies__strong">teaching my own children</strong> this passion.
                                </p>
                                <p>
                                    How about you ? Are you also a ski fanatic, or do you have <strong className="hobbies__strong">drive</strong> for an other sport ?
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="hobbies__accordion-panel problem-solving" onClick={() => handleClick("problem-solving")}>
                        <h3 id="panel3-heading" className="hobbies__panel-heading">
                            <button
                                className="hobbies__accordion-trigger"
                                aria-controls="panel3-content"
                                aria-expanded="false"
                            >
                                <div className="hobbies__accordion-icon-container">
                                    <svg viewBox="5 5 90 90" stroke="currentColor" className="hobbies__accordion-icon">
                                        <g strokeWidth="6" strokeLinecap="round" strokeLinejoin="round" fill="none">
                                            <path className="hobbies__accordion-icon__problemsolving-head" d="m 30 95 l 0 -20 a 1 1 0 0 1 40 -60 q 13 10 20 45 l -12 0 l 0 17 l -12 0 l 0 20" />
                                            <path strokeWidth="4" className="hobbies__accordion-icon__problemsolving-cog" d="M 44 45 a 1 1 0 0 1 12 0 a 1 1 0 0 1 -12 0 m -1.5 -15 l 0 -7.5 l 15 0 l 0 7.5 l 2 1.5 l 6.24 -4.1625 l 8.3175 12.48 l -6.24 4.1625 l 0 2.25 l 6.24 4.1625 l -8.3175 12.48 l -6.24 -4.1625 l -2 1.5 l 0 7.5 l -15 0 l 0 -7.5 l -2 -1.5 l -6.24 4.1625 l -8.3175 -12.48 l 6.24 -4.1625 l 0 -2.25 l -6.24 -4.1625 l 8.3175 -12.48 l 6.24 4.1625 l 2 -1.5" />
                                        </g>
                                    </svg>
                                </div>
                                <span id="panel3-title" className="hobbies__panel-title">Problem Solving</span>
                            </button>
                        </h3>
                        <div
                            className="hobbies__accordion-content"
                            id="panel3-content"
                            aria-labelledby="panel3-heading"
                            aria-hidden="true"
                            role="region"
                        >
                            <div className="hobbies__inside-content">
                                <p>
                                    Some Problem Solving thingies
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="hobbies__accordion-panel designing" onClick={() => handleClick("designing")}>
                        <h3 id="panel4-heading" className="hobbies__panel-heading">
                            <button
                                className="hobbies__accordion-trigger"
                                aria-controls="panel4-content"
                                aria-expanded="false"
                            >
                                <div className="hobbies__accordion-icon-container">

                                    <svg viewBox="0 0 100 100" className="hobbies__accordion-icon">
                                        <g fill="none" strokeLinecap="round" strokeWidth="4">
                                            <g strokeLinejoin="round">
                                                <path className="hobbies__accordion-icon__design-ring1" d="m79.7 20.3c-19.7 24.5-39 47.8-43.4 43.4-2.98-2.98-6.83-6.83-6.83-13.6-0.0137-8.92 5.75-16.8 14.2-19.6 8.5-2.72 17.8 0.364 23 7.63 5.18 7.27 5.05 17.1-0.302 24.2" />
                                                <path className="hobbies__accordion-icon__design-ring2" d="m22.3 49.5c0.00687-17.1 14.4-27.5 27.7-27.5 11.5-0.0998 22.1 7.92 26.4 18.9 4.82 12.6 6e-3 26.8-11.5 33.9-10.6 6.51-22.1-4.57-28.6-11.1-4.36-4.36 18.9-23.7 43.4-43.4" />
                                                <path className="hobbies__accordion-icon__design-ring3" d="m18 76.4c6.34-3.12 6.25-4.91 6.7-7.92 0.31-2.06 0.629-3.45 2-4.82 2.73-2.73 6.9-2.72 9.63 8e-3 7.37 7.66-3.92 18.8-11.3 11.3-13.3-13.4-13.8-35-1.03-49 12.8-14 34.3-15.5 48.8-3.41 14.6 12.1 17.1 33.5 5.78 48.6" />
                                            </g>
                                            <path className="hobbies__accordion-icon__design-cloud" d="m13.2 68.1c0 5.71 4.63 10.3 10.3 10.3 0.255-9e-5 0.51-0.01 0.764-0.0285 0.259 5.82 6.65 10.5 14.4 10.6 3.54 0.0467 6.95-0.886 9.58-2.62 0.994 3.08 3.86 5.17 7.1 5.17 3.68-1e-5 6.81-2.68 7.37-6.32 1.62 0.867 3.43 1.32 5.26 1.32 6.16 1e-4 11.1-4.99 11.1-11.1 5e-5 -6.16-4.99-11.1-11.1-11.1-0.606 9e-5 -1.21 0.0496-1.81 0.148 0.0776-0.526 0.117-1.06 0.117-1.59 1.1e-4 -6-4.86-10.9-10.9-10.9-2.44-2e-5 -4.82 0.823-6.73 2.34-0.117-7.47-6.2-13.5-13.7-13.5-0.0118-1.6e-5 -0.0237-1.6e-5 -0.0355 0l4e-5 -6.6e-5c-7.54 0.0196-13.6 6.14-13.6 13.7 0 1.13 0.14 2.26 0.417 3.35-4.98 0.835-8.63 5.15-8.63 10.2 0.0711 3.61 1.58 7.4 4.84 8.35 9.49 2.68 15.2 1.35 18.3-2.28 2.54-2.92 2.72-7.72-7e-3 -10.4" />
                                        </g>
                                    </svg>

                                </div>
                                <span id="panel4-title" className="hobbies__panel-title">Designing</span>
                            </button>
                        </h3>
                        <div
                            className="hobbies__accordion-content"
                            id="panel4-content"
                            aria-labelledby="panel4-heading"
                            aria-hidden="true"
                            role="region"
                        >
                            <div className="hobbies__inside-content">
                                <p>
                                    Some beautiful design art
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="hobbies__accordion-panel gaming" onClick={() => handleClick("gaming")}>
                        <h3 id="panel5-heading" className="hobbies__panel-heading">
                            <button
                                className="hobbies__accordion-trigger"
                                aria-controls="panel5-content"
                                aria-expanded="false"
                            >
                                <div className="hobbies__accordion-icon-container">
                                    <svg viewBox="0 0 120 120" stroke="currentColor" className="hobbies__accordion-icon">
                                        <g className="hobbies__accordion-icon__gaming-controller" transform="translate(-2.65 -6.01)">
                                            <g transform="matrix(2.03 0 0 2.03 -230 107)">
                                                <path d="m133-36.4c-10.7-3e-6 -16.9 31.5-7.9 31.5 4.63 0 8.03-4.75 10.4-7.13 2.32-2.36 15.4-2.34 17.8-3e-6 2.34 2.34 5.74 7.13 10.4 7.13 9 4e-6 2.81-31.5-7.9-31.5-5.52 0-5.52 3.35-11.4 3.35s-5.84-3.35-11.4-3.35z" fill="none" strokeWidth="4" />
                                                <g strokeWidth="0">
                                                    <circle cx="156" cy="-27.4" r="2.17" />
                                                    <circle cx="156" cy="-19.2" r="2.17" />
                                                    <circle cx="152" cy="-23.3" r="2.17" />
                                                    <circle cx="160" cy="-23.3" r="2.17" />
                                                </g>
                                                <g strokeWidth="0">
                                                    <rect x="128" y="-25" width="11" height="3.5" ry=".601" />
                                                    <rect transform="rotate(90)" x="-28.8" y="-135" width="11" height="3.5" ry=".601" />
                                                </g>
                                            </g>
                                        </g>
                                        <path className="hobbies__accordion-icon__gaming-mouse" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="6" d="m 60 35 c 55 -45 80 48 20 60 a 3 2 0 0 0 -40 0 a 3 2 0 0 0 40 0 M 60 95 l 10 0" />
                                        <path className="hobbies__accordion-icon__gaming-keyboard" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="4" d="M 20 65 l 0 -30 l 80 0 l 0 10 l -80 0 l 0 10 l 80 0 l 0 10 l -80 0 l 10 0 l 0 -30 l 10 0 l 0 30 l 40 0 l 0 -30 l 10 0 l 0 30 l 10 0 l 0 -30 l -30 0 l 0 20 l -10 0 l 0 -20 l -10 0 l 0 20" />
                                    </svg>
                                </div>
                                <span id="panel5-title" className="hobbies__panel-title">Gaming</span>
                            </button>
                        </h3>
                        <div
                            className="hobbies__accordion-content"
                            id="panel5-content"
                            aria-labelledby="panel5-heading"
                            aria-hidden="true"
                            role="region"
                        >
                            <div className="hobbies__inside-content">
                                <p>
                                    As a child all you ever want to do is play. I'm still a child in a way, although i have broaden my sense of playing, I still love playing board games and video games.
                                </p>
                                <p>
                                    Although I love playing board games, it is often quite troublsome finding engaged players, this is why I usually play video games more often.
                                    The main reason why I love video games is the challenge. Most video games have a wide variety of skills to master in order to overcome them. What drives me is the sense of accomplishment when succeeding in a game, after investing a lot of time in mastering the required skills building up the physical reflexes and mental sharpness to finally beat the game or other players, is really rewarding to me, and gives me a strong sense of satisfaction.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                <h4>Coming Soon...</h4>
            </section>
        </div>
    )
}