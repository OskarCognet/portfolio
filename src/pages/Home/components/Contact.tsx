import "./styles/contact.css"

export const Contact = () => {
    return (
        <section id="contact" className="contact container">
            <h2 className="contact__title">Contact</h2>
            <p className="contact__description">
                If you are interested in my profile or just want to say hi, please feel free to contact me by e-mail, text message or phone, I will get back to you as soon as possible.
            </p>
            <div className="contact__info">
                <p className="contact__info-name">E-mail : <br />Phone :</p>
                <p className="contact__info-data">oskarcognet@gmail.com <br />+33 7 81 13 09 11</p>
            </div>
        </section>
    )
}