import "./styles/about.css"

export const About = () => {

    const pl_list = [
        "Python",
        "Java",
        "JavaScript",
        "TypeScript",
        "HTML/CSS",
        "bash",
        "C",
    ]

    const ot_list = [
        "Git",
        "ReactJS",
        "NodeJS",
        "SpringBoot",
        "Office"
    ]

    const sl_list = [
        "French",
        "English",
        "German",
        "Spanish",
        "Polish",
        "Japanese"
    ]

    return (
        <div className="bg-orange">
            <h2 id="about" className="about__title container">About Me</h2>
            <section className="about container">
                <div className="about__description about__grid-item">
                    <h3 className="about__subtitle">Description</h3>
                    <div className="about__paragraph">
                        <p className="about__p-title">Looking for an internship</p>
                        As a 5th year student in <strong className="about__strong">Software Conception and Big Data</strong>, I'm looking for a 6-month end of study internship in the fields of full-stack development, software conception or artificial intelligence.
                    </div>
                    <div className="about__paragraph">
                        <p className="about__p-title">My soft skills</p>
                        I have excellent communication skills and I know how to synthesize information in order to deliver the key points. I'm always willing to make my peer's job easier. I'm a <strong className="about__strong">fast learner</strong> and I can adapt quickly. I'm told to be very reliable. I am pragmatic and detail-oriented, I like to <strong className="about__strong">do things the right way</strong> and come up with ingenious solutions.
                    </div>
                    <div className="about__paragraph">
                        <p className="about__p-title">About this Portfolio</p>
                        I am currently taking on the challenge of building this portfolio to experiment, learn and <strong className="about__strong">improve my front-end coding skills</strong>.
                    </div>
                </div>
                <div id="education" className="about__education about__grid-item">
                    <h3 className="about__subtitle">Education</h3>
                    <div className="about__paragraph">
                        <p className="about__p-title">Engineering Master's degree</p>
                        <p className="about__p-location">CPE Lyon, Lyon</p>
                        <p className="about__p-date">September 2019 - Present</p>
                        <p>
                            <b>Majors :</b> Computer Science, Big Data, Software Development, Web Development, Mathemathics <br />
                            <b>Minors :</b> Electronics, Signal Processing, Physics <br />
                            <b>Options :</b> Economics, Management, Communication
                        </p>
                    </div>
                    <div className="about__paragraph">
                        <p className="about__p-title">CPGE</p>
                        <p className="about__p-location">Lycée Berthollet, Annecy</p>
                        <p className="about__p-date">September 2017 - June 2019</p>
                        <p>2 years full-time higher education in preparation for competitive entry exams to engineering schools</p>
                    </div>
                    <div className="about__paragraph">
                        <p className="about__p-title">Baccalaureate</p>
                        <p className="about__p-location">Lycée Vaugelas, Chambéry</p>
                        <p className="about__p-date">September 2014 - July 2017</p>
                        <p>French high-school scientific baccalaureate passed with honours</p>
                    </div>
                </div>
                <div id="work-experience" className="about__work about__grid-item">
                    <h3 className="about__subtitle">Work Experience</h3>
                    <div className="about__paragraph">
                        <p className="about__p-title">Internship - Test Automation Engineer</p>
                        <p className="about__p-location">Maxon GmbH, Bad Homburg - Germany</p>
                        <p className="about__p-date">October 2022 - July 2023</p>
                        <div className="about__p-description">
                            <ul>
                                <li>Conceived and developed a GUI test automation framework for Maxon's products</li>
                                <li>Developed text and image recognition functions</li>
                                <li>Learned the fundamentals of CI/CD and Agile development</li>
                                <li>Evaluated the Anka tool for its potential use in Maxon's current CI/CD systems</li>
                            </ul>
                        </div>
                    </div>
                    <div className="about__paragraph">
                        <p className="about__p-title">Internship - Biomedical Engineer Assistant</p>
                        <p className="about__p-location">Médipôle de Savoie, Chambéry / Challes-les-Eaux</p>
                        <p className="about__p-date"> July 2021 - August 2022</p>
                        <div className="about__p-description">
                            <ul>
                                <li>Evaluated Atex zones (explosion risk assessment)</li>
                                <li>Managed inventory</li>
                                <li>Managed administrative tasks and archives</li>
                                <li>Participated in various meetings regarding the clinic's management, renovation and expansion</li>
                            </ul>
                        </div>
                    </div>
                    <div className="about__paragraph">
                        <p className="about__p-title">Internship - Factory Worker</p>
                        <p className="about__p-location">Hafner Savoie, Francin</p>
                        <p className="about__p-date">July 2020 - August 2022</p>
                        <div className="about__p-description">
                            <ul>
                                <li>
                                    Key member in the chain of production, packing and delivering of the final product to the customer
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="skills" className="about__skills about__grid-item">
                    <h3 className="about__subtitle">Skills</h3>
                    <div className="about__skills-wrapper">
                        <div className="about__skill">
                            <div className="about__skill-header">Programming Languages :</div>
                            <ul className="about__skill-items">
                                {pl_list.map(
                                    (pl) =>
                                        <li aria-label="code snippet icon" className="about__li" key={pl}>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className="about__svg" fill="#890495" stroke="#890495">
                                                <path fillRule="evenodd" strokeWidth={1} d="M14.447 3.026a.75.75 0 0 1 .527.921l-4.5 16.5a.75.75 0 0 1-1.448-.394l4.5-16.5a.75.75 0 0 1 .921-.527ZM16.72 6.22a.75.75 0 0 1 1.06 0l5.25 5.25a.75.75 0 0 1 0 1.06l-5.25 5.25a.75.75 0 1 1-1.06-1.06L21.44 12l-4.72-4.72a.75.75 0 0 1 0-1.06Zm-9.44 0a.75.75 0 0 1 0 1.06L2.56 12l4.72 4.72a.75.75 0 0 1-1.06 1.06L.97 12.53a.75.75 0 0 1 0-1.06l5.25-5.25a.75.75 0 0 1 1.06 0Z" clipRule="evenodd" />
                                            </svg>
                                            <p>{pl}</p>
                                        </li>
                                )}
                            </ul>
                        </div>
                        <div className="about__skill">
                            <div className="about__skill-header">Other Technologies :</div>
                            <ul className="about__skill-items">
                                {ot_list.map(
                                    (ot) =>
                                        <li aria-label="gear icon" className="about__li" key={ot}>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className="about__svg" fill="#890495">
                                                <path fillRule="evenodd" d="M11.078 2.25c-.917 0-1.699.663-1.85 1.567L9.05 4.889c-.02.12-.115.26-.297.348a7.493 7.493 0 0 0-.986.57c-.166.115-.334.126-.45.083L6.3 5.508a1.875 1.875 0 0 0-2.282.819l-.922 1.597a1.875 1.875 0 0 0 .432 2.385l.84.692c.095.078.17.229.154.43a7.598 7.598 0 0 0 0 1.139c.015.2-.059.352-.153.43l-.841.692a1.875 1.875 0 0 0-.432 2.385l.922 1.597a1.875 1.875 0 0 0 2.282.818l1.019-.382c.115-.043.283-.031.45.082.312.214.641.405.985.57.182.088.277.228.297.35l.178 1.071c.151.904.933 1.567 1.85 1.567h1.844c.916 0 1.699-.663 1.85-1.567l.178-1.072c.02-.12.114-.26.297-.349.344-.165.673-.356.985-.57.167-.114.335-.125.45-.082l1.02.382a1.875 1.875 0 0 0 2.28-.819l.923-1.597a1.875 1.875 0 0 0-.432-2.385l-.84-.692c-.095-.078-.17-.229-.154-.43a7.614 7.614 0 0 0 0-1.139c-.016-.2.059-.352.153-.43l.84-.692c.708-.582.891-1.59.433-2.385l-.922-1.597a1.875 1.875 0 0 0-2.282-.818l-1.02.382c-.114.043-.282.031-.449-.083a7.49 7.49 0 0 0-.985-.57c-.183-.087-.277-.227-.297-.348l-.179-1.072a1.875 1.875 0 0 0-1.85-1.567h-1.843ZM12 15.75a3.75 3.75 0 1 0 0-7.5 3.75 3.75 0 0 0 0 7.5Z" clipRule="evenodd" />
                                            </svg>
                                            <p>{ot}</p>
                                        </li>
                                )}
                            </ul>
                        </div>
                        <div className="about__skill">
                            <div className="about__skill-header">Speaking Languages :</div>
                            <ul className="about__skill-items">
                                {sl_list.map(
                                    (sl) =>
                                        <li aria-label="chat bubbles icon" className="about__li" key={sl}>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className="about__svg" fill="#890495">
                                                <path d="M4.913 2.658c2.075-.27 4.19-.408 6.337-.408 2.147 0 4.262.139 6.337.408 1.922.25 3.291 1.861 3.405 3.727a4.403 4.403 0 0 0-1.032-.211 50.89 50.89 0 0 0-8.42 0c-2.358.196-4.04 2.19-4.04 4.434v4.286a4.47 4.47 0 0 0 2.433 3.984L7.28 21.53A.75.75 0 0 1 6 21v-4.03a48.527 48.527 0 0 1-1.087-.128C2.905 16.58 1.5 14.833 1.5 12.862V6.638c0-1.97 1.405-3.718 3.413-3.979Z" />
                                                <path d="M15.75 7.5c-1.376 0-2.739.057-4.086.169C10.124 7.797 9 9.103 9 10.609v4.285c0 1.507 1.128 2.814 2.67 2.94 1.243.102 2.5.157 3.768.165l2.782 2.781a.75.75 0 0 0 1.28-.53v-2.39l.33-.026c1.542-.125 2.67-1.433 2.67-2.94v-4.286c0-1.505-1.125-2.811-2.664-2.94A49.392 49.392 0 0 0 15.75 7.5Z" />
                                            </svg>
                                            <p>{sl}</p>
                                        </li>
                                )}
                            </ul>
                        </div>
                    </div>
                </div>
            </section >
        </div>
    )
}

/*
About me

Profesional Paragraph
Education - Work Experience
Projects
Skills

Personal Paragraph
Hobbies
- description
- photos


*/