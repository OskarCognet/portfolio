import { Button } from "../../../components/Button"
import "./styles/hero.css"

export const Hero = () => {
    return (
        <section className="hero container">
            <img className="hero__img" src="/home/hero.jpg" alt="Hero profile picture" />
            <h2 className="hero__subtitle">Hi, I'm Oskar 😎</h2>
            <div>
                <h1 className="hero__title">Software Engineer</h1>
                <h1 className="hero__title">&</h1>
                <h1 className="hero__title">Full-Stack Web Developer</h1>
            </div>
            <p className="hero__description">
                I am a <strong>software engineer</strong> student, newly passionate about <strong>full-stack development</strong>, and long lasting <strong>UI/UX fanatic</strong>.
                I am eager to build outstanding websites and applications, by always giving <strong>special attention to details</strong>.
            </p>
            <a href="#contact" style={{ textDecoration: "none" }}>
                <Button className="hero__button">
                    Contact Me
                </Button>
            </a>
        </section>
    )
}