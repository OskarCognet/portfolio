# Welcome to my web CV project.

The website is currently available at : https://portfolio-r33gvby0c-oskar-cognets-projects.vercel.app

You can also:
 - clone this project : `git clone https://gitlab.com/OskarCognet/portfolio.git`
 - run : `npm run dev`
 - open a new browser tab and go to : "http://localhost:5173"

Enjoy !